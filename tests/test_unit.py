import pytest
from sfcli import util

class TestUtil:

    def test_gen_file_name(self):
        name = '~/Downloads/accounts.csv'
        assert util._gen_file_name(name) == 'accounts'
