## SalesforceCLI

This package provides a command line interface for interacting with Salesforce SOQL and bulk creating, updating and deleting records using the batch api, which sends 200 records at a time and multithreading. The default is 4 threads, but this can be changed.

## Authentication

In order to authenticate you will need your username, password and security token. These should be put in a file called .env in your root directory with the keys specified below. If you are connecting to a sandbox you will also need to include a variable specifying that environment.

```
.env

SALESFORCE_USERNAME = {username}
SALESFORCE_PASSWORD = {password}
SALESFORCE_SECURITY_TOKEN = {security_token}
SALESFORCE_DOMAIN = test
```

## setenv

If you want to be able to connect to multiple environments duplicate these variables with a keyword after them.

```
.env

SALESFORCE_USERNAME_PROD = {username}
SALESFORCE_PASSWORD_PROD = {password}
SALESFORCE_SECURITY_TOKEN_PROD = {security_token}
```

You must have the default variables set in order for the CLI to load. You can sitch to other environments by using the setenv command and specifying the keyword.

```
(Cmd) setenv prod
```

## getenv

This will print out the keyword name of the environment you are currently using.

## setversion

Changes the API verison you are using to the version you input. The default is 44.0

```
(Cmd) 42.0
```

## select/SELECT

Run a SOQL query and have the results printed in columns in the terminal.

SELECT * FROM is supported. The code will retrieve all of the fields on the object and then run the query. This can look messy in the terminal, so it is recommended to use download with SELECT *.

```
(Cmd) select id, name from account limit 100
```

## download

Takes a SOQL query as input, but logs the results in a .csv saved in your downloads folder.

```
(Cmd) download select id, name from account limit 100
```

## objects

By default this will print the names of all of the objects in your Salesforce. You can add an optional parameter, which will be used to filter the results to only ones with that input string in them.

```
(Cmd) objects order
```

## fields

This takes the name of the object as an input. You can add an optional parameter, which will be used to filter the results to only ones with that input string in them.

```
(Cmd) fields order na
```

## setthreads

This allows you to set the number of threads you want used on the create, update and delete processes.

```
(Cmd) setthreads 8
```

## end

This will revoke the access token being used and close out of the shell.

## create

This creates records in Salesforce using the batch api and by default 4 threads, so 800 records at a time. The results are logged in successes and failures spreadsheet in your downloads folder.

This method takes the object name and the file name/path if it is not in your current directory.

The file must be a .csv and the column headers must match the API names of the fields they correspond to in Salesforce

```
(Cmd) create account accounts.csv
```

## update

This updates records in Salesforce using the batch api and by default 4 threads, so 800 records at a time. The results are logged in successes and failures spreadsheet in your downloads folder.

This method takes the object name and the file name/path if it is not in your current directory.

The file must be a .csv and the column headers must match the API names of the fields they correspond to in Salesforce. One of the columns must have the id header and contain the Salesforce record id.

```
(Cmd) update account accounts.csv
```

## delete

This deletes records in Salesforce using the batch api and by default 4 threads, so 800 records at a time. The results are logged in successes and failures spreadsheet in your downloads folder.

This method takes the file name/path if it is not in your current directory.

The file must contain one column with the id header.

```
(Cmd) delete accounts.csv
```
